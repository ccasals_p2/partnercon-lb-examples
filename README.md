# PartnerCon Layout Builder examples

This repo contains some snippets, scrubbed from working prod code. 
It is not intended to be used as a standalone Drupal instance (which is why it's missing tons of stuff).

Included you will find:
* an example of a custom layout plugin, to allow a layout to be configurable
* a layouts.yml file, for declaring our own layouts
* an example of a content view block to particle pattern theme layer
* an example of a paragraph pattern to particle pattern theme layer
* an example of a block pattern to particle pattern theme layer
* a hook_theme_suggestion_alter to add more useful block hooks for 
theming custom block types.

Link to the [matching slide deck](https://docs.google.com/presentation/d/1eOaRmlsPq4YN27lnbmlRuO2QHZtnT9zmUwKIaWqwpWY/edit?usp=sharing).

Thanks!
