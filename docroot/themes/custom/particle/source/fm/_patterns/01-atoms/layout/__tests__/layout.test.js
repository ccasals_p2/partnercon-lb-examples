import { name } from '..';

test('layout component is registered', () => {
  expect(name).toBe('layout');
});
