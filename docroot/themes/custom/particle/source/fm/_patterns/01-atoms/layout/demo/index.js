/**
 * Demo of layout. Pulls in layout assets, and provides demo-only assets.
 *
 * This file is NOT imported by the design system, but is included as part of apps/app-name/index.js
 */

// Import component assets
import 'atoms/layout';

// Import demo assets
import twig from './layouts.twig';
import yaml from './layouts.yml';
import markdown from './layouts.md';

export default {
  twig,
  yaml,
  markdown,
};
