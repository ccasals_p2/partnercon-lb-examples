/**
 * Demo of featuredPromo. Pulls in featuredPromo assets, and provides demo-only assets.
 *
 * This file is NOT imported by the design system, but is included as part of apps/app-name/index.js
 */

// Import component assets
import 'molecules/featured-promo';

// Import demo assets
import twig from './featured-promos.twig';
import yaml from './featured-promos.yml';
import markdown from './featured-promos.md';
import './featured-promo-image.png';

export default {
  twig,
  yaml,
  markdown,
};
