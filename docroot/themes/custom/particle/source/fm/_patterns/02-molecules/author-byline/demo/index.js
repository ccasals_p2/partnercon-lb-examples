/**
 * Demo of insightDetail. Pulls in insightDetail assets, and provides demo-only assets.
 *
 * This file is NOT imported by the design system, but is included as part of apps/app-name/index.js
 */

// Import component assets
import 'molecules/author-byline';

// Import demo assets
import twig from './author-bylines.twig';
import yaml from './author-bylines.yml';
import markdown from './author-bylines.md';
import './author-byline.png';

export default {
  twig,
  yaml,
  markdown,
};
