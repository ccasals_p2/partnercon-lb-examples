import { name } from '..';

test('author-byline component is registered', () => {
  expect(name).toBe('authorByline');
});
