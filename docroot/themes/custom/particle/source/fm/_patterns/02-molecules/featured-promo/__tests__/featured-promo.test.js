import { name } from '..';

test('featured-promo component is registered', () => {
  expect(name).toBe('featuredPromo');
});
