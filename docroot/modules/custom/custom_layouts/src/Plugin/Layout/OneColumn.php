<?php

namespace Drupal\custom_layouts\Plugin\Layout;

/**
 * Used for most layout configurations.
 */
class OneColumn extends LayoutsBase {

  /**
   * Components of the config form.
   *
   * @var array
   */
  private $formItems = ['placemat', 'full_bleed'];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $this->setFormElements($this->formItems);
    return parent::defaultConfiguration();
  }

}
