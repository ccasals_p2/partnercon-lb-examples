<?php

namespace Drupal\custom_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * For more on layout plugins, see https://www.drupal.org/project/layouts.
 *
 * See core /layouts/src/Plugin/Layout/DefaultConfigLayout.php for example.
 */
class LayoutsBase extends LayoutDefault implements PluginFormInterface {

  /**
   * Array of form elements to be added to config form.
   *
   * @var array
   */
  private $formElements = [];

  /**
   * Set form elements for a given layout.
   *
   * @param array $elements
   *   The form elements to use in config.
   */
  public function setFormElements(array $elements) {
    $this->formElements = $elements;
  }

  /**
   * Get form elements for given layout.
   *
   * @return array
   *   The form elements set in config.
   */
  public function getFormElements() {
    return $this->formElements;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaultElements = $this->getFormElements();
    $defaults = [];

    foreach ($defaultElements as $name) {
      switch ($name) {
        case 'color_classes':
          $defaults[$name] = 'transparent';
          break;

        case 'placemat':
          $defaults[$name] = 'none';
          break;

        case 'full_bleed':
          $defaults[$name] = '';
          break;

        case 'column_width':
          $defaults[$name] = 'two-col';
          break;

        case 'column_count':
          $defaults[$name] = 'two';
          break;

      }
    }

    return parent::defaultConfiguration() + $defaults;

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $formElements = $this->getFormElements();

    foreach ($formElements as $name) {
      switch ($name) {
        case 'color_classes':
          $form['color_classes'] = [
            '#type' => 'radios',
            '#title' => $this->t('Highlight Color'),
            '#default_value' => $configuration['color_classes'],
            '#options' => [
              'transparent' => $this->t('None'),
              'gray' => $this->t('Gray'),
              'blue' => $this->t('Blue'),
              'teal' => $this->t('Teal'),
              'magenta' => $this->t('Magenta'),
              'orange' => $this->t('Orange'),
              'yellow' => $this->t('Yellow'),
              'green' => $this->t('Green'),
            ],
          ];
          break;

        case 'placemat':
          $form['placemat'] = [
            '#type' => 'radios',
            '#required' => 'true',
            '#title' => $this->t('Placemat options'),
            '#description' => $this->t("Adds an optional grey placemat
             background element. Comes in left or right-handed orientations"),
            '#default_value' => $configuration['placemat'],
            '#options' => [
              'right' => $this->t('Right corner cut'),
              'left' => $this->t('Left corner cut'),
              'none' => $this->t('No placemat'),
            ],
          ];
          break;

        case 'full_bleed':
          $form['full_bleed'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Full width'),
            '#default_value' => $configuration['full_bleed'],
            '#description' => $this->t("If checked, this section
                will extend to the full width of the viewport"),
            '#return_value' => 'break-grid',
          ];
          break;

        case 'column_width':
          $form['column_width'] = [
            '#type' => 'radios',
            '#title' => $this->t('Column width'),
            '#default_value' => $configuration['column_width'],
            '#required' => 'true',
            '#options' => [
              'two-col' => $this->t('Equal width (50/50)'),
              'main-left' => $this->t('Main left, right rail'),
              'main-right' => $this->t('Main right, left rail'),
            ],
          ];
          break;

        case 'column_count':
          $form['column_count'] = [
            '#type' => 'radios',
            '#title' => $this->t('Column count'),
            '#default_value' => $configuration['column_count'],
            '#options' => [
              'two' => $this->t('Two column with right rail'),
              'one' => $this->t('One center aligned column'),
            ],
          ];
          break;

      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $formElements = $this->getFormElements();

    foreach ($formElements as $name) {
      switch ($name) {
        case 'color_classes':
          $this->configuration['color_classes'] = $form_state->getValue('color_classes');
          break;

        case 'full_bleed':
          $this->configuration['full_bleed'] = $form_state->getValue('full_bleed');
          break;

        case 'placemat':
          $this->configuration['placemat'] = $form_state->getValue('placemat');
          break;

        case 'column_width':
          $this->configuration['column_width'] = $form_state->getValue('column_width');
          break;

        case 'column_count':
          $this->configuration['column_count'] = $form_state->getValue('column_count');
          break;

      }
    }
  }

}
