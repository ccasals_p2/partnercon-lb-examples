<?php

namespace Drupal\custom_layouts\Plugin\Layout;

/**
 * Used for most layout configurations.
 */
class ThreeColumn extends LayoutsBase {

  /**
   * Components of the config form.
   *
   * @var array
   */
  private $formItems = ['placemat', 'color_classes'];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $this->setFormElements($this->formItems);
    return parent::defaultConfiguration();
  }

}
