<?php

namespace Drupal\custom_layouts\Plugin\Layout;

/**
 * Used for most layout configurations.
 */
class Masthead extends LayoutsBase {

  /**
   * Components of the config form.
   *
   * @var array
   */
  private $formItems = ['column_count'];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $this->setFormElements($this->formItems);
    return parent::defaultConfiguration();
  }

}
